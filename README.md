**Install**

cd anticaptcha folder

npm update

npm install request
npm install request-promise 


---

## Usage

```javascript

var anticaptchaGooglecaptcha = require("anticaptcha-googlecaptcha")
//default value
const siteDetails = {
    sitekey: '6LccSjEUAAAAANCPhaM2c-WiRxCZ5CzsjR_xxx', // key site google Captcha
    pageurl: 'https://ct.captcha-delivery.com', // url destination
};
 
const api = {
    in: 'https://api.anti-captcha.com/createTask',
    res: 'https://api.anti-captcha.com/getTaskResult',
    key: '4e785dab78a5dcxxxxx', // account key at anti captcha
    pollingInterval: 5000
};
 
const params = {
    key: api.key,
    type: 'RecaptchaV2TaskProxyless', // google v2
    websiteKey: siteDetails.sitekey,
    websiteURL: siteDetails.pageurl,
 
    isInvisible:false,
    userAgent:'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36'
};
(async () => {
 
    try {
           // solve the captcha with API
        console.log("* Waiting g-recaptcha-reponse");
        const token = await anticaptchaGooglecaptcha.solveCaptchaV2(params, api);
        console.log('* Receive g-recaptcha-reponse: ' + token);
        
        // unsccessful case 
        if (token.includes('ERROR') || token.includes('CAPCHA_NOT_READY')){
            process.exit(1)
        }
 
    } catch (error) {
        console.log(error);
    }
   
  })();

```

---

## Motivation

These days captchas are unfortunately everywhere, with reCAPTCHA having the biggest "market share" in that space (> 80%). The situation got really bad, with privacy minded users (tracking blocker, VPNs) being penalized heavily and having to solve a lot of reCAPTCHA challenges constantly while browsing the web.

These small codes allow us to get automatically google token from Anti Captcha Service and bypass google's protection. It is very important for crawler's activites.

---