/**
 * @Author: Ha Pham The Anh
 * @Date: 21/2/2020
 */

// module should be installed:
// npm i request-promise
// npm i modern-random-ua 
const puppeteer = require('puppeteer');
const Apify = require('apify');

const { solveGeeTest } = require('./captchaapi');


const siteDetails = {
    pageurl: 'https://www.carsales.com.au/cars/?q=Service.carsales.&offset=36'
 };


const api = {
    in: 'https://api.anti-captcha.com/createTask',
    res: 'https://api.anti-captcha.com/getTaskResult',
    key: '123456', // account key at anti captcha
    pollingInterval: 5000
};

const params = {
    key: api.key,
    type: 'GeeTestTaskProxyless',
    gt:'1e505deed3832c02c96ca5abe70df9ab',
    websiteURL: siteDetails.pageurl,
    geetestApiServerSubdomain:'api-na.geetest.com'
};


(async () => {

    const browser = await puppeteer.launch({
        headless: false,
        userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.3945.130 Safari/537.36',
        slowMo: 250,
        args: [
             '--disable-web-security',
             '--disable-features=IsolateOrigins,site-per-process',
             '--user-data-dir=./.config/google-chrome'
        ],
        ignoreDefaultArgs: ["--disable-extensions","--enable-automation"],
    });   

    // const page = await browser.newPage();

    const context = await browser.createIncognitoBrowserContext();
    const page = await context.newPage();     // Create a new page in a pristine context.


    await page.setViewport({ 
        width: 1024 + Math.floor(Math.random() * 100),
        height: 768 + Math.floor(Math.random() * 100)
    });

    try {

        await page.setRequestInterception(true);
        page.on('request', (request) => {
            const url = request.url();
            const filters = [
                'api-na.geetest.com',
            ];
            const shouldAbort = filters.some((urlPart) => url.includes(urlPart));
            
            if (shouldAbort) 
                request.abort();
            else {
                request.continue();
            }
        });

        await page.goto(siteDetails.pageurl);

        // waiting loaded iFrame
        await page.waitForSelector('iframe');
        const iframe = page.mainFrame().childFrames()[0];
       
        let str = await iframe.content();
        // console.log(str);

        let arr = str.split(",");
        for (i=0; i < arr.length ; i ++){
            if(arr[i].includes(" challenge:") > 0){
                arr_challenge = arr[i].split(":");
                params.challenge = arr_challenge[1].replace(/[^0-9a-zA-Z]/g,'');
            }
        }

        console.log(params);

        const token = await solveGeeTest(params, api);
        console.log(token);

        const elementHandle = await page.$('iframe');
        const frameContentFrame = await elementHandle.contentFrame();
        await frameContentFrame.evaluate(async (captchaResult) => {

            geetestResponse = {
                geetest_challenge: captchaResult.challenge,
                geetest_validate: captchaResult.validate,
                geetest_seccode: captchaResult.seccode,
            };
            captchaCallback();

        }, token);

        await Apify.utils.sleep(5 * 1000);

        const content = await page.content();        
        console.log(content);

        await browser.close(); //close browser

    } catch (error) {
        console.log('ERR:', error.message);

    }

    
   
  })();
