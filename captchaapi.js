const request = require('request-promise')
const getAnswer = (id, api) => {
    return new Promise((resolve, reject) => {

        const polling = setInterval(() => {

            var requestData = {
                "clientKey": api.key,
                "softId":961,
                "taskId":id
            };

            request.post(api.res, {
                json: true,
                body: (typeof requestData === 'object') ? requestData : null
            })
            .then((res) => {
                if (res.errorId === 0 && res.status==='ready') {
                    clearInterval(polling)
                    resolve(res.solution);                    
                } else if (res.errorId !== 0 ) {
                    clearInterval(polling)
                    reject(res.errorCode)
                }

                // console.log(res);
            })
            .catch((error) => {
                clearInterval(polling)
                reject(error)
            })
        }, api.pollingInterval)

    })
}

const submitCaptchaV2 = (captchaParams, api) => {
    return new Promise((resolve, reject) => {

        var requestData = {
            "clientKey": api.key,
            "softId":961,
            "task":
                {
                    "type": captchaParams.type,
                    "websiteURL":captchaParams.websiteURL,
                    "websiteKey":captchaParams.websiteKey,
                    "websiteSToken":captchaParams.websiteSToken != null ? captchaParams.websiteSToken : '',
                    "recaptchaDataSValue": captchaParams.recaptchaDataSValue != null ? captchaParams.recaptchaDataSValue: '',
                    "proxyType":captchaParams.proxyType != null ? captchaParams.proxyType : '',
                    "proxyAddress":captchaParams.proxyAddress != null ? captchaParams.proxyAddress : '',
                    "proxyPort":captchaParams.proxyPort != null ? captchaParams.proxyPort : 0,
                    "proxyLogin":captchaParams.proxyLogin != null ? captchaParams.proxyLogin: '',
                    "proxyPassword":captchaParams.proxyPassword != null ? captchaParams.proxyPassword: '',
                    "userAgent":captchaParams.userAgent != null ? captchaParams.userAgent: '',
                    "isInvisible":captchaParams.isInvisible != null ? captchaParams.isInvisible:false,
                    "cookies":captchaParams.cookies != null ? captchaParams.cookies: ''
                }
        };
          

        request.post(api.in, {
            json: true,
            body: (typeof requestData === 'object') ? requestData : null
        })
        .then((res) => {
            if (res.errorId === 0) {
                resolve(res.taskId)
            } else {
                reject(res.errorCode)
            }

            console.log(res);
        })
        .catch((error) => {
            reject({ error })
        })
    })
}

const submitGeeTest = (captchaParams, api) => {
    return new Promise((resolve, reject) => {

        var requestData = {
            "clientKey": api.key,
            "softId":961,
            "task":
                {
                    "type": captchaParams.type,
                    "websiteURL":captchaParams.websiteURL,
                    "gt":captchaParams.gt,
                    "challenge":captchaParams.challenge,
                    "geetestApiServerSubdomain":captchaParams.geetestApiServerSubdomain
                }
        };
          

        request.post(api.in, {
            json: true,
            body: (typeof requestData === 'object') ? requestData : null
        })
        .then((res) => {
            if (res.errorId === 0) {
                resolve(res.taskId)
            } else {
                reject(res.errorCode)
            }

            // console.log(res);
        })
        .catch((error) => {
            reject({ error })
        })
    })
}

const submitHCaptcha = (captchaParams, api) => {
    return new Promise((resolve, reject) => {

        var requestData = {
            "clientKey": api.key,
            "softId":961,
            "task":
                {
                    "type": captchaParams.type,
                    "websiteURL":captchaParams.websiteURL,
                    "websiteKey":captchaParams.websiteKey
                }
        };
          

        request.post(api.in, {
            json: true,
            body: (typeof requestData === 'object') ? requestData : null
        })
        .then((res) => {
            if (res.errorId === 0) {
                resolve(res.taskId)
            } else {
                reject(res.errorCode)
            }

            // console.log(res);
        })
        .catch((error) => {
            reject({ error })
        })
    })
}

const solveCaptchaV2 = async (params, api) => {
    try {
        let id = await submitCaptchaV2(params, api);
        console.log("* Id task:" + id);

        let answer = await getAnswer(id, api);
        return answer
    } catch (e) {
        return e
    }
}
const solveGeeTest = async (params, api) => {
    try {
        let id = await submitGeeTest(params, api);
        console.log("* Id task:" + id);

        let answer = await getAnswer(id, api);
        return answer
    } catch (e) {
        return e
    }
}

const solveHCaptcha = async (params, api) => {
    try {
        let id = await submitHCaptcha(params, api);
        console.log("* Id task:" + id);

        let answer = await getAnswer(id, api);
        return answer.gRecaptchaResponse;
    } catch (e) {
        return e
    }
}

module.exports = {
    getAnswer,
    solveCaptchaV2,
    solveGeeTest,
    solveHCaptcha
}